
<!DOCTYPE html>
<html>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!------ Include the above in your HEAD tag ---------->
    
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                
                
                @if($errors->has('name'))
                     <div class="alert alert-danger">{{$errors->first('name')}}</div>
                @endif
                <form method="post" action="{{ route('passwordProcess') }}">
                    @csrf 
                <input type="hidden" name="resetToken" value="<?php echo $_GET['token'];?>">
                <input type="hidden" name="email" value="<?php echo $_GET['email'];?>">
                <div class="form-group">
                    <label for="exampleInputPassword1">New Password</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="New Password" name="password">
                  </div>
                    <div class="form-group">
                      <label for="exampleInputPassword1">Confirm Password</label>
                      <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Confirm Password" name="password_confirmation">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Update Password</button>
                  </form>
                
            </div>  
        </div>
    </div>
</html>