@component('mail::message')
# {{ config('app.name') }}

Reset or change your password.

@component('mail::button', ['url' => 'http://127.0.0.1:8000/response-password-reset?token='.$token.'&email='.$email])
Change Password
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
