<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubOption extends Model
{
    use HasFactory;
    protected $table = 'sub_options';
    public function category(){
        return $this->belongsTo(MainCategory::class,'category_id');
    }
    
}
