<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DrinkCategory extends Model
{
    use HasFactory;
    public function DrinkDetails(){
        return $this->hasMany(DrinkDetails::class);
    }
}
