<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HealthyFoodCategory extends Model
{
    use HasFactory;
    public function posts(){
        return $this->hasMany(DietPlanPost::class,'hf_category_id');
    }
    public function sub_options_id(){
        return $this->belongsTo(SubOption::class,'sub_options_id');
    } 
}
