<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DrinkDetails extends Model
{
    use HasFactory;
    protected $fillable=['user_id','category_id','drink_cat_id','ml'];

    public function drink_cat(){
        return $this->belongsTo(DrinkCategory::class,'drink_cat_id')->select('id','name');
    } 
}
