<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DietPlanPost extends Model
{
    use HasFactory;
    protected $with=['ingredients'];
    public function category(){
        return $this->belongsTo(HealthyFoodCategory::class,'hf_category_id');
    }
    public function ingredients(){
        return $this->hasMany(Ingredients::class,'diet_plan_posts_id');
    }
}
