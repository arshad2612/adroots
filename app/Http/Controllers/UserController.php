<?php

namespace App\Http\Controllers;

use App\Models\CalculatorLogs;
use App\Models\Emoji;
use App\Models\HeartRate;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Twilio\Jwt\ClientToken;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Support\Arr;
use Symfony\Component\HttpFoundation\Response;

class UserController extends Controller
{

    // login information
    public function authenticate(Request $request)
    {

        if(is_numeric($request->get('email'))){
            $credentials = ['phone'=>$request->get('email'),'password'=>$request->get('password')];
        }
        elseif (filter_var($request->get('email'), FILTER_VALIDATE_EMAIL)) {
            $credentials = ['email' => $request->get('email'), 'password'=>$request->get('password')];
        }else{
            $credentials = ['username' => $request->get('email'), 'password'=>$request->get('password')];
        }

        $credentials = Arr::add($credentials, 'isVerified', '1');

        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 400);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }
        $user = auth()->user();

        return response()->json([
            'message' => 'User successfully Login',
            'token' => $token,
            'Login User' => $user
        ], 200);

    }


    // signup function
     public function register(Request $request)
     {

        Validator::extend('spaces', function($attr, $value){
            return preg_match('/^\S*$/u', $value);
        });

        $validator = Validator::make($request->all(), [
            'username' => 'required|spaces|unique:users,username',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|unique:users,phone',
            'password' => 'required|string|min:6|confirmed',
            'role' => 'required',
        ],
        [
            'username.spaces' => 'Username whitespace not allowed.'
        ]
        );

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $code = rand(1000, 9999); //generate random code
        $request['verification_code'] = $code; //add code in $request body
        $phone = $request->get('phone');

        $this->sendSms($phone, $code);

        $user = User::create([
             'username' => $request->get('username'),
             'email' => $request->get('email'),
             'phone' => $request->get('phone'),
             'password' => Hash::make($request->get('password')),
             'role' => $request->get('role'),
             'verification_code' => $code,
        ]);
        $token = JWTAuth::fromUser($user);
        $recentUser = User::latest()->first();

        return response()->json([
             'message' => 'Varification Code Send successfully',
             'token' => $token,
             'user' =>$recentUser
        ], 201);
     }

     //send varification code sms
    public function sendSms($phone, $code)
    {

        $accountSid = config('app.twilio')['TWILIO_ACCOUNT_SID'];
        $authToken = config('app.twilio')['TWILIO_AUTH_TOKEN'];
        try
        {
            $client = new Client(['auth' => [$accountSid, $authToken]]);
            $result = $client->post('https://api.twilio.com/2010-04-01/Accounts/'.$accountSid.'/Messages.json',
            ['form_params' => [
                'Body' => 'Abundantly Developed Roots - Verification Code: '. $code, //set message body
                'To' => $phone,
                'From' => '+16509999964'    //we get this number from twilio
            ]]);
            return $result;
        }
        catch (Exception $e)
        {
            echo "Error: " . $e->getMessage();
        }
    }


     // verified code
     public function verifyCode(Request $request){

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'code' => 'required'
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $code = $request->get('code');
        $id = $request->get('id');

        $varified = User::where([
            ['id','=', $id],
            ['verification_code','=', $code]
        ])->latest()->first();

        if($varified){
            $varified->isVerified   = true;
            $varified->save();
            return response()->json([
                'message' => 'Successfully Varified'
            ], 200);

        }else{
            return response()->json([
                'message' => 'Invalid Code'
            ], 400);
        }
    }

    //forgot password
    public function sendPasswordResetEmail(Request $request){

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        //
        if($this->validEmail($request->email)) {
          // If email exists
          $user = $this->validEmail($request->email);

          return response()->json([
              'message' => 'Email Exists.',
              'user' =>$user
          ], Response::HTTP_OK);
        } else {
            //If email does not exist
            return response()->json([
                'message' => 'Email does not exist.'
            ], Response::HTTP_NOT_FOUND);
        }
    }

    //validate user email function
    public function validEmail($email) {
        return User::where('email', $email)->first();
    }

    //update user password
    public function update_password(Request $request,$id){

        $validator = Validator::make($request->all(), [
            'password' => 'required|string|min:6'

            ]);
        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }
        $user=User::find($id);

        $user->password=Hash::make($request->password);
        $user->save();
        return response()->json([
            'message' => 'User Password Updated!'
        ], 200);
    }

    // update user profile
    public function update (Request $request, $id){

        $validator = Validator::make($request->all(), [
            'fullname' => 'required|string',
            'gender' => 'required',
            'dob' => 'required|date',
            'age' => 'required|numeric',
            'weight' => 'required',
            'height_ft' => 'required',
            'height_in' => 'required',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(), 400);
        }

        $height = $request->height_ft.'.'.$request->height_in;

        $user           = User::find($id);
        $user->fullname = $request->fullname;
        $user->gender   = $request->gender;
        $user->dob      = $request->dob;
        $user->age      = $request->age;
        $user->weight   = $request->weight;
        $user->height   = $height;
        $user->save();

        return response()->json([
            'message' => 'User profile successfully updated!',
            'User' =>$user
        ], 200);
    }

    public function insert_emoji(Request $request){
            $validator=Validator::make($request->all(),[
                'category_id'=>'required|numeric',
                'emoji'=>'required|string',
                'inserted_at'=>'required|date'
             ]);
            if($validator->fails()){
                return response()->json($validator->errors(), 400);
            }
            Emoji::create([
                'category_id'=>$request->get('category_id'),
                'emoji'=>$request->get('emoji'),
                'inserted_at'=>$request->get('inserted_at'),
                ]);
                return response()->json([
                    'message' => 'Emoji SuccessFully inserted'
                ], 201);

    }
    public function get_emoji(Request $request){

        $validator=Validator::make($request->all(),[
            'date'=>'required'
         ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $date=$request->get('date');
        $date=explode('-',$date);
        $start=$date[1]."-".$date[0]."-01 00:00:00";
        $end=$date[1]."-".$date[0]."-31 00:00:00";
        // $query="SELECT * FROM `emoji` WHERE `inserted_at` BETWEEN $start AND $end;";
        // print_r($query);die;

        return response()->json([
            'message'=>'Feelings Through out the month',
            'data'=>Emoji::select('*')->whereBetween('inserted_at',[$start,$end])->get()
        ],200);
    }
    public function insert_heart_rate(Request $request){
        $validator=Validator::make($request->all(),[
            'category_id'=>'required|numeric',
            'heart_rate'=>'required|numeric',
            'inserted_at'=>'required|date'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        HeartRate::create([
            "category_id"=>$request->get('category_id'),
            "heart_rate"=>$request->get('heart_rate'),
            "inserted_at"=>$request->get('inserted_at')
        ]);
        return response()->json([
            'message' => 'Heart Rate  SuccessFully inserted'
        ], 201);
    }
    public function get_heartbeat(Request $request){

        $validator=Validator::make($request->all(),[
            'date'=>'required'
         ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $date=$request->get('date');
        $date=explode('-',$date);
        $start=$date[1]."-".$date[0]."-01";
        $end=$date[1]."-".$date[0]."-31";
        // $query="SELECT * FROM `emoji` WHERE `inserted_at` BETWEEN $start AND $end;";
        // print_r($query);die;

        return response()->json([
            'message'=>'HeartBeat Through out the month',
            'data'=>HeartRate::select('*')->whereBetween('inserted_at',[$start,$end])->get()
        ],200);
    }

    public function insert_logs(Request $request){
        $validator=Validator::make($request->all(),[
            'user_id'=>'required|integer',
            'calc_id'=>'required|integer',
            'value'=>'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        CalculatorLogs::create([
            'user_id'=>$request->get('user_id'),
            'calc_id'=>$request->get('calc_id'),
            'value'=>$request->get('value')
        ]);
        return response()->json([
            'message'=>'Calculator Log Inserted'

        ],201);

    }


}
