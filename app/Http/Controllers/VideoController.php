<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VideoCategory;
use App\Models\Video;
use Illuminate\Support\Facades\DB;


class VideoController extends Controller
{
    // fetch Videos Category
    public function index(){
        
        $data = VideoCategory::all();
        return response()->json([
            'message' => 'Get Videos Categories',
            'Video Categories' => $data
        ], 200);  
    }

    //fetch all videos 
    public function allVideos(){
        $videos = Video::all();
        return response()->json([
            'message' => 'Get All Videos',
            'Videos' => $videos
        ], 200);
    }

    //fetch single Video
    public function singleVideo($id){
        $video = Video::find($id);
        $views =  $video->views + 1;
        $video->views =  $views;
        $video->save();

        return response()->json([
            'message' => 'Fetch  Single Videos',
            'Video' => $video
        ], 200);
    }

    //fetch category wise videos
    public function categorywisevideos($id){

        $category = DB::table('video_categories')->where('id', $id)->pluck('name');
        $videos = Video::where('category_id', $id)->get();
        
        return response()->json([
            'message' => 'Get Category wise Videos',
            'Video Category' => $category,
            'Videos' => $videos,
        ], 200);
            
    }


}
