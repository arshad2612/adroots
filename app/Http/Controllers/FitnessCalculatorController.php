<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator as FacadesValidator;

class FitnessCalculatorController extends Controller
{
    //

    public function calculate_bmi(Request $request){
        $validator = FacadesValidator::make($request->all(), [
            'gender' => 'required',
            'height' => 'required',
            'weight' => 'required' 
             ]
        );
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $gender=$request->get('gender');
        $height=cm_to_m($request->get('height'));
        $weight=$request->get('weight');
        $data=$this->BMI_calculation($height,$weight);
        return response()->json([
            'message' => 'BMI Calculation',
            'BMI' => $data
       ], 200);
    }
    public function calculate_bmr(Request $request){
        $validator = FacadesValidator::make($request->all(), [
            'gender' => 'required',
            'age'    => 'required',
            'height' => 'required',
            'weight' => 'required' 
             ]
        );
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $gender=$request->get('gender');//gender:male,female
        $height_cm=$request->get('height');//height in (KG)
        $weight_kg=$request->get('weight');//weight in (cm)
        $age=$request->get('age');//age
        $data=$this->BMR_calculation($gender,$age,$height_cm,$weight_kg);
        return response()->json([
            'message' => 'BMR Calculation',
            'BMR' => $data
       ], 200);
    }
    //////////////////////////////
    //calculations and formulas//
    ////////////////////////////

    public function BMI_calculation($height_m,$weight_kg){
        $height_square=pow($height_m,2);
        
        return $weight_kg/$height_square;
    }
    public function BMR_calculation($gender,$age,$height_cm,$weight_kg){
        /*where:
            W is body weight in kg
            H is body height in cm
            A is age
            F is body fat in percentage

        Mifflin-St Jeor Equation:
            For men:
            BMR = 10W + 6.25H - 5A + 5
            For women:
            BMR = 10W + 6.25H - 5A - 161
         */
        if($gender=='male'){
            return 10*$weight_kg+6.25*$height_cm-5*$age+5;
        }elseif($gender=='female'){
            return 10*$weight_kg+6.25*$height_cm-5*$age-161;
        }    
    }

}
