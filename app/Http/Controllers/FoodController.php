<?php

namespace App\Http\Controllers;

use App\Models\DrinkDetails;
use App\Models\HealthyFoodCategory;
use App\Models\DietPlanPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class FoodController extends Controller
{
    //
    public function show_drinks(){
        $drinks=DB::table('drink_categories')->get();
        return response()->json([
            'message' => 'Get All Drink Options',
            'drinks' => $drinks
        ], 200);
    }
    public function add_drinks(Request $request){
        $validator=Validator::make($request->all(),[
            'user_id'=>'required|integer',
            'category_id'=>'required|integer',
            'drink_cat_id'=>'required|integer',
            'ml'=>'required'
        ]);
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        $data=DrinkDetails::create([
            "user_id"=>$request->get('user_id'),
            "category_id"=>$request->get('category_id'),
            "drink_cat_id"=>$request->get('drink_cat_id'),
            "ml"=>$request->get('ml')
        ]);
        return response()->json([
            "message"=>"Drink details added"
        ],201);
    }
    public function get_drink_info(Request $request){
        $validator=Validator::make($request->all(),[
            'user_id'=>'required',
            'category_id'=>'required'
        ]);
        //return invalid json 
        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        //values assignment
        $start_date=date('Y-m-d 00:00:00',time());
        $end_date= date('Y-m-d 23:59:59',time());
        $category_id=$request->get('category_id');
        $user_id=$request->get('user_id');
        $data=DrinkDetails::with('drink_cat')->where('category_id',$category_id)->where('user_id',$user_id)->whereBetween('created_at',[$start_date,$end_date])->get();
        $total_ml=0;
        $coke_ml=0;
        $water_ml=0;
        $juice_ml=0;
        $coffee_ml=0;
        $total_glass=0;
        if(count($data)==0){
            return response()->json([
                "message"=>"No Drinks found in a day",
                "data"=>0
            ],200);
        }
        foreach($data as $row){
            $total_glass++;
            $total_ml+=$row['ml']; 
            if($row['drink_cat']['name']=='Coke'){
                $coke_ml+=$row['ml'];
                return $coke_ml;
            }elseif($row['drink_cat']['name']=='Water'){
                $water_ml+=$row['ml'];
            }elseif($row['drink_cat']['name']=='Juice'){
                $juice_ml+=$row['ml'];
            }elseif($row['drink_cat']['name']=='Coffee'){
                $coffee_ml+=$row['ml'];
            }
        }
        $coke_percentage=($coke_ml/$total_ml)*100;
        $water_percentage=($water_ml/$total_ml)*100;
        $juice_percentage=($juice_ml/$total_ml)*100;
        $coffee_percentage=($coffee_ml/$total_ml)*100;
        return response()->json([
            "message"=>"all drinks in a day",
            "percentage"=>[
                "Coke"=>$coke_percentage,
                "Water"=>$water_percentage,
                "Juice"=>$juice_percentage,
                "Coffee"=>$coffee_percentage
            ],
            "t_glass"=>$total_glass,
            "data"=>$data

        ],200);
    }
    public function get_post_info(){
        
        $food_categories=HealthyFoodCategory::withCount('posts')->with('sub_options_id')->get();
        $posts=DietPlanPost::with('ingredients')->get();
        return response()->json([
            'message' => 'All Dishes and posts',
            'categories' => $food_categories,
            'posts'=>$posts
        ], 200);
    }
    public function post_info_id($id){
        $data=DietPlanPost::find($id);
        return response()->json([
            'message'=>'',
            'data'=>$data
        ],200);
    }
}
