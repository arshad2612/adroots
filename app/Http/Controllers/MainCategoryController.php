<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MainCategory;
use App\Models\Option;
use App\Models\SubOption;
use App\Models\Calculator;
use Illuminate\Support\Facades\DB;

class MainCategoryController extends Controller
{
    // fetch all stores with products
    public function index(){
        
        $data = MainCategory::all();
        return response()->json([
            'message' => 'Get All Main Options',
            'Category' => $data
        ], 200);
        
    }

    public function showOptions($id){

        $categorydata = DB::table('main_categories')->where('id', $id)->first();
        $category = $categorydata->name;
        $option = Option::where('category_id', $id)->get();
        
        return response()->json([
            'message' => 'Get All Main Options',
            'Category Name' => $category,
            'Options' => $option,
        ], 200);
            
    }

    public function subOptions($id){
        $optiondata = DB::table('options')->where('id', $id)->first();
        $option = $optiondata->name;
        $sub_option = SubOption::where('option_id', $id)->get();

        return response()->json([
            'message' => 'Get All Sub Options',
            'Option Name' => $option,
            'Options' => $sub_option,
        ], 200);
    }


    public function showCalculators($id){
        $Calnamedata = DB::table('sub_options')->where('id', $id)->first();
        $Calname = $Calnamedata->name; 
        $calculators = Calculator::where('cal_id', $id)->get();

        return response()->json([
            'message' => 'Get All Calculators',
            'Option Name' => $Calname,
            'Calculator' => $calculators,
        ], 200);


    }
}
