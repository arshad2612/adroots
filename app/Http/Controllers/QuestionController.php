<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\QuestionCategory;
use App\Models\Question;


class QuestionController extends Controller
{
     // fetch all stores with products
     public function index(){
        
        $data = QuestionCategory::with("questions")->get();
        return response()->json([
            'message' => 'Get All Questions',
            'Questions' => $data
        ], 200);
        
    }
}
