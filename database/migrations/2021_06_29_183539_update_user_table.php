<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('fullname');
            $table->string('gender');
            $table->string('phone');
            $table->string('dob');
            $table->string('age');
            $table->string('height');
            $table->string('weight');
            $table->string('photo');
            $table->string('verification_code');
            $table->boolean('isVerified')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('fullname','gender','phone','dob','age','height','weight','photo','verification_code','isVerified');
        });
    }
}
