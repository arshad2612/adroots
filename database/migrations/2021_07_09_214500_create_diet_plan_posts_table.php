<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDietPlanPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('diet_plan_posts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('hf_category_id');
            $table->string('title');
            $table->text('description');
            $table->double('protein');
            $table->double('calories');
            $table->double('fats');
            $table->double('carbo');
            $table->string('img_path');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('diet_plan_posts');
    }
}
