<?php

namespace Database\Factories;

use App\Models\DietPlanPost;
use Illuminate\Database\Eloquent\Factories\Factory;

class DietPlanPostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DietPlanPost::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "hf_category_id"=>$this->faker->numberBetween(1,3),
            "title"=>$this->faker->sentence,
            "description"=>$this->faker->paragraph(),
            "protein"=>$this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            "calories"=>$this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            "fats"=>$this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            "carbo"=>$this->faker->randomFloat($nbMaxDecimals = NULL, $min = 0, $max = NULL),
            "img_path"=>$this->faker->imageUrl($width=200,$height=200)
        ];
    }
}
