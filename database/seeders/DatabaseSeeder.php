<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        /*Healthy Food Factories */
        //truncate values
        \App\Models\HealthyFoodCategory::truncate();
        \App\Models\DietPlanPost::truncate();
        \App\Models\Ingredients::truncate();
        \App\Models\HealthyFoodCategory::create([
            "sub_options_id"=>4,
            "name"=>"Vegetable",
            "img_path"=>"http://localhost:8000/storage/app/public/HealthyFoodCategory/Vegetable.png"
        ]);
        \App\Models\HealthyFoodCategory::create([
            "sub_options_id"=>4,
            "name"=>"Mushroom",
            "img_path"=>"http://localhost:8000/storage/app/public/HealthyFoodCategory/Mushroom.png"
        ]);
        \App\Models\HealthyFoodCategory::create([
            "sub_options_id"=>4,
            "name"=>"Fruits",
            "img_path"=>"http://localhost:8000/storage/app/public/HealthyFoodCategory/Fruit.png"
        ]);
        for($i=0;$i<10;$i++){
            /*Post Factories */
            $post=\App\Models\DietPlanPost::factory()->create();
            \App\Models\Ingredients::factory(3)->create([
                "diet_plan_posts_id"=>$post->id
            ]);
        }

    }
}
