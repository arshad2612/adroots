<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('response-password-reset', 'App\Http\Controllers\ChangePasswordController@changePassword');
Route::post('resetPassword', 'App\Http\Controllers\ChangePasswordController@passwordResetProcess')->name('passwordProcess'); // Reset Password
