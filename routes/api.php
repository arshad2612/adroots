<?php

use App\Http\Controllers\FitnessCalculatorController;
use App\Http\Controllers\FoodController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\MainCategoryController;
use App\Http\Controllers\VideoController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//**** without required token routes *****//
Route::post('register', [UserController::class, 'register']); // create user Account
Route::post('verifycode', [UserController::class, 'verifyCode']);    // varified code
Route::post('login', [UserController::class, 'authenticate']); // login


//******forgot password******//
Route::post('sendPasswordResetLink', 'App\Http\Controllers\PasswordResetRequestController@sendPasswordResetEmail'); // Send Link

//forgot password
Route::post('passwordReset', [UserController::class, 'sendPasswordResetEmail']); // Reset password
Route::post('password/{id}', [UserController::class, 'update_password']);      // update password


//Token Required APIs
Route::group(['middleware' => ['jwt.verify']], function() {

    Route::post('profile/{id}', [UserController::class, 'update']);      // update user profile


    //questions and answer screen
    Route::get('questions', [QuestionController::class, 'index']);      // update user profile

    //Home Screens
    Route::get('home', [MainCategoryController::class, 'index']); //Main Category (Screen: 11-ADR-Home)
    Route::get('option/{id}', [MainCategoryController::class, 'showOptions']); //Main Category (Screen: 14-ADR-Options)
    Route::get('suboption/{id}', [MainCategoryController::class, 'subOptions']); //Main Category (Screen: 15-ADR-Health Calculators)

    //Calculators
    Route::get('calculator/{id}', [MainCategoryController::class, 'showCalculators']); //Calculators (Screen: 16,17,18-ADR- Calculators)
    Route::post('bmi', [FitnessCalculatorController::class, 'calculate_bmi']); //Calculators (Screen: 16,17,18-ADR- Calculators)
    Route::post('bmr', [FitnessCalculatorController::class,'calculate_bmr']); //Calculators BMR (Screen: )

    //Videos Routes
    Route::get('browsevideos', [VideoController::class, 'index']); //Videos Category (Screen: 11-ADR-Home)
    Route::get('videos', [VideoController::class, 'allVideos']); //Videos Category (Screen: 11-ADR-Home)
    Route::get('video/{id}', [VideoController::class, 'singleVideo']); //Videos Category (Screen: 11-ADR-Home)
    Route::get('videocategory/{id}', [VideoController::class, 'categorywisevideos']); //Videos Category (Screen: 11-ADR-Home)
    //Drink Food Category
    Route::get('getdrinks',[FoodController::class,'show_drinks']);// Drinks List: 30-ADR-What did you drink?
    Route::post('addDrink', [FoodController::class,'add_drinks']);// Drinks List: 30-ADR-What did you drink?
    Route::get('getDrinkInfo', [FoodController::class,'get_drink_info']);//29-ADR-Stay Hydrated
    //post categories
    Route::get('getposts',[FoodController::class,'get_post_info']);//26-ADR-Want to eat healthy food?
    Route::get('getpost/{id}',[FoodController::class,'post_info_id']);//26-ADR-Want to eat healthy food?
    //mood emojis insertion
    Route::post('insertemoji',[UserController::class,'insert_emoji']);//44-ADR-How are you feeling today?
    Route::post('getemoji',[UserController::class,'get_emoji']);//45-ADR-Mood Graph
    //heart beat insertion based on calender
    Route::post('heartrate',[UserController::class,'insert_heart_rate']);//48-ADR-Record your Heart Rate
    Route::post('getheart',[UserController::class,'get_heartbeat']);//48-ADR-Record your Heart Rate
    //Calculator Logs
    Route::post('calclogs',[UserController::class,'insert_logs']);

});
